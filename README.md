# conception_solution_embarquee_temps_reel_i2_multitache_connectivite

## Mise en place
Vous devez posséder la dernière version de vagrant ainsi que VirtualBox et son extension pack.

Pour installer la VM, exécuter la commande suivante : `vagrant up` dans un terminal.

Une fois l'installation fini, exécutez la commande `vagrant reload` pour redémarrer la VM puis `vagrant ssh` pour vous connecter en ssh à la VM.
> Remarque: Sur windows, vous devez, avant de vous connecter en ssh à la VM, exécuter la commande `set VAGRANT_PREFER_SYSTEM_BIN=0` à chaque lancement du terminal.